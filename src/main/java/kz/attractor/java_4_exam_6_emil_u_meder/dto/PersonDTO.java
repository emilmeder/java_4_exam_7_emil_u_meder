package kz.attractor.java_4_exam_6_emil_u_meder.dto;
import kz.attractor.java_4_exam_6_emil_u_meder.model.*;
import lombok.*;

import java.util.List;

@Data
@Builder(access = AccessLevel.PRIVATE)
@AllArgsConstructor(access = AccessLevel.PRIVATE)
@NoArgsConstructor(access = AccessLevel.PRIVATE, force = true)
public class PersonDTO {

    public static PersonDTO from(Person person) {
        return builder()
                .id(person.getId())
                .name(person.getName())
                .mail(person.getMail())
                .build();
    }

    private String id;
    private String name;
    private String mail;

}