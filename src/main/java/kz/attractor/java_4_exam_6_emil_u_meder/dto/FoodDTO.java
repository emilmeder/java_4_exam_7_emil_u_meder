package kz.attractor.java_4_exam_6_emil_u_meder.dto;
import kz.attractor.java_4_exam_6_emil_u_meder.model.*;
import lombok.*;

import java.util.List;

@Data
@Builder(access = AccessLevel.PRIVATE)
@AllArgsConstructor(access = AccessLevel.PRIVATE)
@NoArgsConstructor(access = AccessLevel.PRIVATE, force = true)
public class FoodDTO {

    public static FoodDTO from(Food food) {
        return builder()
                .id(food.getId())
                .name(food.getName())
                .type(food.getType())
                .price(food.getPrice())
                .build();
    }

    private String id;
    private String name;
    private int price;
    private String type;


}