package kz.attractor.java_4_exam_6_emil_u_meder.dto;
import kz.attractor.java_4_exam_6_emil_u_meder.model.*;
import lombok.*;

import java.util.List;

@Data
@Builder(access = AccessLevel.PRIVATE)
@AllArgsConstructor(access = AccessLevel.PRIVATE)
@NoArgsConstructor(access = AccessLevel.PRIVATE, force = true)
public class PlaceDTO {

    public static PlaceDTO from(Place place) {
        return builder()
                .id(place.getId())
                .name(place.getName())
                .describe(place.getDescribe())
                .build();
    }

    private String id;
    private String name;
    private String describe;


}