package kz.attractor.java_4_exam_6_emil_u_meder.dto;
import kz.attractor.java_4_exam_6_emil_u_meder.model.*;
import lombok.*;

import java.time.LocalDateTime;
import java.util.List;

@Data
@Builder(access = AccessLevel.PRIVATE)
@AllArgsConstructor(access = AccessLevel.PRIVATE)
@NoArgsConstructor(access = AccessLevel.PRIVATE, force = true)
public class OrderDTO {

    public static OrderDTO from(Order order) {
        return builder()
                .id(order.getId())
                .person(order.getPerson())
                .food(order.getFood())
                .date(order.getDate())

                .build();
    }

    private String id;
    //    private List<Person> person = new ArrayList<>();
    private  Person person;
    private Food food;
    private LocalDateTime date;


}