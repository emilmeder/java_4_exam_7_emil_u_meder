package kz.attractor.java_4_exam_6_emil_u_meder;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Java4Exam6EmilUMederApplication {

    public static void main(String[] args) {
        SpringApplication.run(Java4Exam6EmilUMederApplication.class, args);
    }

}
