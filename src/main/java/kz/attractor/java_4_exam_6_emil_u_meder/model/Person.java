package kz.attractor.java_4_exam_6_emil_u_meder.model;
import kz.attractor.java_4_exam_6_emil_u_meder.util.GenerateData;
import kz.attractor.java_4_exam_6_emil_u_meder.util.SecurityConfig;
import lombok.Builder;
import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.Collection;
import java.util.List;


@Document
@Data
@Builder
public class Person implements UserDetails {
    @Id
    private String id;
    private String name;
    private String mail;
    private String password;


    public static Person makeNew() {
        return builder()
                .name(GenerateData.randomPersonName())
                .mail(GenerateData.randomEmail())
                .password(SecurityConfig.encoder().encode("qqw"))
                .build();
    }
    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return List.of(new SimpleGrantedAuthority("FULL"));
    }

    @Override
    public String getUsername() {
        return getMail();
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return true;
    }

    public void setPassword(String password) {
        this.password = SecurityConfig.encoder().encode(password);
    }

}
