package kz.attractor.java_4_exam_6_emil_u_meder.model;

import kz.attractor.java_4_exam_6_emil_u_meder.util.GenerateData;
import lombok.Builder;
import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;


@Document
@Data
@Builder
public class Order {
    @Id
    private String id;
//    private List<Person> person = new ArrayList<>();
    private  Person person;
    private Food food;
    private LocalDateTime date;


//    public static Order makeNew() {
//        Random random = new Random();
//        return builder()
//                .date(LocalDateTime.now())
//                .build();
//    }

    public static Order makeNew(Person person, Food food) {
        return builder()
                .date(LocalDateTime.now())
                .person(person)
                .food(food)
                .build();
    }
}