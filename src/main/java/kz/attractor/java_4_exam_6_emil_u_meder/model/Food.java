package kz.attractor.java_4_exam_6_emil_u_meder.model;

import kz.attractor.java_4_exam_6_emil_u_meder.util.GenerateData;
import lombok.Builder;
import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;


@Document
@Data
@Builder
public class Food {
    @Id
    private String id;
    private String name;
    private int price;
    private String type;
    private Place place;


    public static Food makeNew(Place place) {
        Random random = new Random();
        return builder()
                .place(place)
                .name(GenerateData.randomDish().getName())
                .type(GenerateData.randomDish().getType())
                .price(random.nextInt(100)+40)
                .build();
    }
}
