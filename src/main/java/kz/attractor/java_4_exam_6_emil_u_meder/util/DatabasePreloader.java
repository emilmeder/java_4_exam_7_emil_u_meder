package kz.attractor.java_4_exam_6_emil_u_meder.util;
import kz.attractor.java_4_exam_6_emil_u_meder.model.*;
import kz.attractor.java_4_exam_6_emil_u_meder.repository.*;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.stream.Stream;

import static java.util.stream.Collectors.toList;

@Configuration
public class DatabasePreloader {
        private static final Random random = new Random();

        @Bean
        CommandLineRunner initDatabase(PersonRepository pr, OrderRepository or, PlaceRepository placer, FoodRepository fr) {
            return (args) -> {
                pr.deleteAll();
                or.deleteAll();
                placer.deleteAll();
                fr.deleteAll();

                Person test = Person.makeNew();
                test.setMail("qqw@gmail.com");
//                                                        password is "qqw"
                pr.save(test);

                List<Place> places = Stream.generate(Place::makeNew).limit(10).collect(toList());
                placer.saveAll(places);
                placer.findAll().forEach(e -> System.out.println(e));

                List<Person> persons = Stream.generate(Person::makeNew).limit(10).collect(toList());
                pr.saveAll(persons);
                pr.findAll().forEach(e -> System.out.println(e));

                List<Food> foods = Stream.generate(() -> Food.makeNew((places.get(random.nextInt(places.size()))))).limit(25).collect(toList());
                fr.saveAll(foods);

                List<Order> orders = Stream.generate(() -> Order.makeNew(persons.get(random.nextInt(persons.size())), foods.get(random.nextInt(foods.size())))).limit(10).collect(toList());
                or.saveAll(orders);
                or.findAll().forEach(e -> System.out.println(e));


            };
        }
    }
