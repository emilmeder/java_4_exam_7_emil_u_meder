package kz.attractor.java_4_exam_6_emil_u_meder.util;

import kz.attractor.java_4_exam_6_emil_u_meder.service.UserAuthService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

@Configuration
@EnableWebSecurity
public class SecurityConfig extends WebSecurityConfigurerAdapter {
        /*@Override
    protected void configure(
            AuthenticationManagerBuilder auth) throws Exception {
        auth.inMemoryAuthentication()
                .withUser("admin")
                .password(encoder().encode("password"))
                .authorities("FULL")
                .and()
                .withUser("guest")
                .password(encoder().encode("guest"))
                .roles("GUEST")
                .authorities("READ_ONLY");
    }*/


    @Bean
    public static PasswordEncoder encoder() {
        return new BCryptPasswordEncoder();
    }

    protected void configure(HttpSecurity http) throws Exception{
        // Правило 1: Всё, что начинается с /subscriptions
        // должно быть доступно только
        // после авторизации пользователя
        http.authorizeRequests().antMatchers("/person/**").fullyAuthenticated();

//        http.authorizeRequests().antMatchers("/makePublication/*").fullyAuthenticated();
        http.authorizeRequests().antMatchers("/order/**").fullyAuthenticated();
        http.csrf().disable()
                // Правило 2: Разрешить всё остальные запросы
                .authorizeRequests().anyRequest().permitAll()
                .and().httpBasic()
                // Так как мы авторизуемся через заголовок запроса, то
                // форма входа на сайт и выхода с него нам тоже не нужны.
                .and().sessionManagement().disable();
    }
}
