package kz.attractor.java_4_exam_6_emil_u_meder.controller;

import kz.attractor.java_4_exam_6_emil_u_meder.annotations.ApiPageable;
import kz.attractor.java_4_exam_6_emil_u_meder.dto.OrderDTO;
import kz.attractor.java_4_exam_6_emil_u_meder.dto.PlaceDTO;
import kz.attractor.java_4_exam_6_emil_u_meder.service.FoodService;
import kz.attractor.java_4_exam_6_emil_u_meder.service.OrderService;
import kz.attractor.java_4_exam_6_emil_u_meder.service.PersonService;
import kz.attractor.java_4_exam_6_emil_u_meder.service.PlaceService;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Slice;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import springfox.documentation.annotations.ApiIgnore;


@RestController
@RequestMapping("/order")
public class OrderController {
    private final PlaceService placerS;
    private final FoodService fs;
    private final OrderService os;
    private final PersonService ps;

    public OrderController(PlaceService placerS, FoodService fs, OrderService os, PersonService ps) {
        this.placerS = placerS;
        this.fs = fs;
        this.os = os;
        this.ps = ps;
    }

    @ApiPageable
    @GetMapping
    public Slice<OrderDTO> findOrders(@ApiIgnore Pageable pageable) {
        return os.findOrders(pageable);
    }

    @GetMapping("/{place}/{food}/{mail}")
    public String newOrder(@PathVariable("place") String place,@PathVariable ("food") String food,@PathVariable ("mail") String mail){
        return os.newOrder(food, mail);
    }

    @ApiPageable
    @GetMapping("/{mail}")
    public Slice<OrderDTO> findByMail(@ApiIgnore Pageable pageable,@PathVariable("mail") String mail) {
        return os.findByMail(pageable, mail);
    }
}
