package kz.attractor.java_4_exam_6_emil_u_meder.controller;

import kz.attractor.java_4_exam_6_emil_u_meder.annotations.ApiPageable;
import kz.attractor.java_4_exam_6_emil_u_meder.dto.PersonDTO;
import kz.attractor.java_4_exam_6_emil_u_meder.dto.PlaceDTO;
import kz.attractor.java_4_exam_6_emil_u_meder.service.FoodService;
import kz.attractor.java_4_exam_6_emil_u_meder.service.OrderService;
import kz.attractor.java_4_exam_6_emil_u_meder.service.PersonService;
import kz.attractor.java_4_exam_6_emil_u_meder.service.PlaceService;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Slice;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;


@RestController
@RequestMapping("/person")
public class PersonController {
    private final PlaceService placerS;
    private final FoodService fs;
    private final OrderService os;
    private final PersonService ps;

    public PersonController(PlaceService placerS, FoodService fs, OrderService os, PersonService ps) {
        this.placerS = placerS;
        this.fs = fs;
        this.os = os;
        this.ps = ps;
    }
    //    @ApiPageable
//    @GetMapping
//    public Slice<PersonDTO>


    @ApiPageable
    @GetMapping
    public Slice<PersonDTO> findPersons(@ApiIgnore Pageable pageable) {
        return ps.findPersons(pageable);
    }


    @PostMapping(path = "/reg")
    public PersonDTO reg(@RequestBody PersonDTO personDTO) {
        return ps.reg(personDTO);
    }
}
