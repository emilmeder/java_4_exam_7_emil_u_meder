package kz.attractor.java_4_exam_6_emil_u_meder.controller;

import kz.attractor.java_4_exam_6_emil_u_meder.annotations.ApiPageable;
import kz.attractor.java_4_exam_6_emil_u_meder.dto.FoodDTO;
import kz.attractor.java_4_exam_6_emil_u_meder.dto.PersonDTO;
import kz.attractor.java_4_exam_6_emil_u_meder.service.FoodService;
import kz.attractor.java_4_exam_6_emil_u_meder.service.OrderService;
import kz.attractor.java_4_exam_6_emil_u_meder.service.PersonService;
import kz.attractor.java_4_exam_6_emil_u_meder.service.PlaceService;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Slice;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import springfox.documentation.annotations.ApiIgnore;

@RestController
@RequestMapping("/food")
public class FoodController {
    private final PlaceService placerS;
    private final FoodService fs;
    private final OrderService os;
    private final PersonService ps;

    public FoodController(PlaceService placerS, FoodService fs, OrderService os, PersonService ps) {
        this.placerS = placerS;
        this.fs = fs;
        this.os = os;
        this.ps = ps;
    }


    @ApiPageable
    @GetMapping
    public Slice<FoodDTO> findFoods(@ApiIgnore Pageable pageable) {
        return fs.findFoods(pageable);
    }

//    @ApiPageable
//    @GetMapping("/{type}")
//    public Slice<FoodDTO> findByType(@ApiIgnore Pageable pageable,@PathVariable("type") String type) {
//        return fs.findByType(pageable, type);
//    }

    @ApiPageable
    @GetMapping("/{name}")
    public Slice<FoodDTO> findAllByPlaceName(@ApiIgnore Pageable pageable,@PathVariable("name") String name) {
        return fs.findAllByPlaceName(pageable, name);
    }
}
