package kz.attractor.java_4_exam_6_emil_u_meder.service;

import kz.attractor.java_4_exam_6_emil_u_meder.dto.PersonDTO;
import kz.attractor.java_4_exam_6_emil_u_meder.dto.PlaceDTO;
import kz.attractor.java_4_exam_6_emil_u_meder.model.Person;
import kz.attractor.java_4_exam_6_emil_u_meder.repository.FoodRepository;
import kz.attractor.java_4_exam_6_emil_u_meder.repository.OrderRepository;
import kz.attractor.java_4_exam_6_emil_u_meder.repository.PersonRepository;
import kz.attractor.java_4_exam_6_emil_u_meder.repository.PlaceRepository;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Slice;
import org.springframework.stereotype.Service;

@Service
public class PersonService {
    public final PlaceRepository placer;
    public final PersonRepository pr;
    public final OrderRepository or;
    public final FoodRepository fr;

    public PersonService(PlaceRepository placer, PersonRepository pr, OrderRepository or, FoodRepository fr) {
        this.placer = placer;
        this.pr = pr;
        this.or = or;
        this.fr = fr;
    }
    public PersonDTO reg(PersonDTO personDTO) {
        var person = Person.builder()
        .id(personDTO.getId())
        .name(personDTO.getName())
        .mail(personDTO.getMail())
        .build();
        pr.save(person);
        return PersonDTO.from(person);
    }
    public Slice<PersonDTO> findPersons(Pageable pageable) {
        var slice = pr.findAll(pageable);
        return slice.map(PersonDTO::from);
    }


}
