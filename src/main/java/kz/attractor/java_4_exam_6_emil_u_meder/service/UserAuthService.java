package kz.attractor.java_4_exam_6_emil_u_meder.service;

import kz.attractor.java_4_exam_6_emil_u_meder.repository.*;
import kz.attractor.java_4_exam_6_emil_u_meder.model.*;
import lombok.AllArgsConstructor;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
@AllArgsConstructor
public class UserAuthService implements UserDetailsService {
    private final PersonRepository ps;

    @Override
    public Person loadUserByUsername(String userMail) throws UsernameNotFoundException {
        Optional<Person> user = ps.findByMail(userMail);
//        if (user.isPresent())
        if (user.isEmpty()) {
            throw new UsernameNotFoundException("User does not exit");
        }
        return user.get();
    }
}

