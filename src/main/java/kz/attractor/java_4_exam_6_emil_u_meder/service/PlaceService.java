package kz.attractor.java_4_exam_6_emil_u_meder.service;

import kz.attractor.java_4_exam_6_emil_u_meder.dto.PlaceDTO;
import kz.attractor.java_4_exam_6_emil_u_meder.model.Order;
import kz.attractor.java_4_exam_6_emil_u_meder.model.Person;
import kz.attractor.java_4_exam_6_emil_u_meder.model.Place;
import kz.attractor.java_4_exam_6_emil_u_meder.repository.FoodRepository;
import kz.attractor.java_4_exam_6_emil_u_meder.repository.OrderRepository;
import kz.attractor.java_4_exam_6_emil_u_meder.repository.PersonRepository;
import kz.attractor.java_4_exam_6_emil_u_meder.repository.PlaceRepository;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Slice;
import org.springframework.stereotype.Service;

@Service
public class PlaceService {
    public final PlaceRepository placer;
    public final PersonRepository pr;
    public final OrderRepository or;
    public final FoodRepository fr;

    public PlaceService(PlaceRepository placer, PersonRepository pr, OrderRepository or, FoodRepository fr) {
        this.placer = placer;
        this.pr = pr;
        this.or = or;
        this.fr = fr;
    }

    public Slice<PlaceDTO> findPlaces(Pageable pageable) {
        var slice = placer.findAll(pageable);
        return slice.map(PlaceDTO::from);
    }

    public String newOrder(String food1 , String mail1){
        var person =  pr.findByMail(mail1);
        var food = fr.findByName(food1);
        var order = Order.makeNew(person.get(), food.get());
        or.save(order);
        return "Successful order";
    }
}
