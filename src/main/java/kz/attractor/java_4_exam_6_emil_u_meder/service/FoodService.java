package kz.attractor.java_4_exam_6_emil_u_meder.service;

import kz.attractor.java_4_exam_6_emil_u_meder.dto.FoodDTO;
import kz.attractor.java_4_exam_6_emil_u_meder.dto.PlaceDTO;
import kz.attractor.java_4_exam_6_emil_u_meder.repository.FoodRepository;
import kz.attractor.java_4_exam_6_emil_u_meder.repository.OrderRepository;
import kz.attractor.java_4_exam_6_emil_u_meder.repository.PersonRepository;
import kz.attractor.java_4_exam_6_emil_u_meder.repository.PlaceRepository;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Slice;
import org.springframework.stereotype.Service;

@Service
public class FoodService {
    public final PlaceRepository placer;
    public final PersonRepository pr;
    public final OrderRepository or;
    public final FoodRepository fr;

    public FoodService(PlaceRepository placer, PersonRepository pr, OrderRepository or, FoodRepository fr) {
        this.placer = placer;
        this.pr = pr;
        this.or = or;
        this.fr = fr;
    }

    public Slice<FoodDTO> findFoods(Pageable pageable) {
        var slice = fr.findAll(pageable);
        return slice.map(FoodDTO::from);
    }

//    public Slice<FoodDTO> findAllByPlaceName(Pageable pageable, String placeName) {
//        var place = pr.findByName(placeName);

//
//        var slice = fr.findAllByPlaceId(pageable, id);
//        return slice.map(FoodDTO::from);
//    }

////    public Slice<FoodDTO> findByType(Pageable pageable, String name) {
//        var place = pr.findByName(name);
//
//        String id = place.get().getId();
//
//        var slice = fr.findAllByPlaceId(pageable, id);
//
//        return slice.map(FoodDTO::from);
//    }



    public Slice<FoodDTO> findAllByPlaceName(Pageable pageable, String name) {
    var place = pr.findByName(name);

    String id = place.get().getId();

    var slice = fr.findAllByPlaceId(pageable, id);

    return slice.map(FoodDTO::from);
    }




}
