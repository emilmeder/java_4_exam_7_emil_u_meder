package kz.attractor.java_4_exam_6_emil_u_meder.service;

import kz.attractor.java_4_exam_6_emil_u_meder.dto.OrderDTO;
import kz.attractor.java_4_exam_6_emil_u_meder.dto.PlaceDTO;
import kz.attractor.java_4_exam_6_emil_u_meder.model.Order;
import kz.attractor.java_4_exam_6_emil_u_meder.repository.FoodRepository;
import kz.attractor.java_4_exam_6_emil_u_meder.repository.OrderRepository;
import kz.attractor.java_4_exam_6_emil_u_meder.repository.PersonRepository;
import kz.attractor.java_4_exam_6_emil_u_meder.repository.PlaceRepository;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Slice;
import org.springframework.stereotype.Service;

@Service
public class OrderService {
    public final PlaceRepository placer;
    public final PersonRepository pr;
    public final OrderRepository or;
    public final FoodRepository fr;

    public OrderService(PlaceRepository placer, PersonRepository pr, OrderRepository or, FoodRepository fr) {
        this.placer = placer;
        this.pr = pr;
        this.or = or;
        this.fr = fr;
    }

    public String newOrder(String food1 , String mail1){
        var person =  pr.findByMail(mail1);
        var food = fr.findByName(food1);
        var order = Order.makeNew(person.get(), food.get());
        or.save(order);
        return "Successful order";
    }

    public Slice<OrderDTO> findOrders(Pageable pageable) {
        var slice = or.findAll(pageable);
        return slice.map(OrderDTO::from);
    }

    public Slice<OrderDTO> findByMail(Pageable pageable, String mail) {
    var user = pr.findByMail(mail);
    String id = user.get().getId();
    var order = or.findAllByPersonId(id,pageable);
    return order.map(OrderDTO::from);
    }
}
