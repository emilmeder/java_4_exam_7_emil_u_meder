package kz.attractor.java_4_exam_6_emil_u_meder.repository;

import kz.attractor.java_4_exam_6_emil_u_meder.model.Food;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.Optional;

public interface FoodRepository extends PagingAndSortingRepository<Food, String> {
    Page<Food> findAllByPlaceId(Pageable pageable, String id);
    Optional<Food> findByType(String type);

    Optional<Food> findByName(String name);




}
