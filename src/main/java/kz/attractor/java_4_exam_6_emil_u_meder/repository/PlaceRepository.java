package kz.attractor.java_4_exam_6_emil_u_meder.repository;

import kz.attractor.java_4_exam_6_emil_u_meder.model.Place;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.Optional;

public interface PlaceRepository extends PagingAndSortingRepository<Place, String> {
    Optional<Place> findByName(String name);
}
