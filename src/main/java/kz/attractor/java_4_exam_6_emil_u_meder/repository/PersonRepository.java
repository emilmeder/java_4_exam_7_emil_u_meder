package kz.attractor.java_4_exam_6_emil_u_meder.repository;

import kz.attractor.java_4_exam_6_emil_u_meder.model.Person;
import org.springframework.data.repository.PagingAndSortingRepository;
import java.util.List;
import java.util.Optional;

public interface PersonRepository extends PagingAndSortingRepository<Person, String> {
    Optional<Person> findByName(String name);
    Optional<Person> findByMail(String mail);
//    boolean existsByMail(String Mail);
    List<Person> findPersonById(String id);
}