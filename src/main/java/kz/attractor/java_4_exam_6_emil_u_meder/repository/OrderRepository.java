package kz.attractor.java_4_exam_6_emil_u_meder.repository;

import kz.attractor.java_4_exam_6_emil_u_meder.model.Order;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface OrderRepository extends PagingAndSortingRepository<Order, String> {
    Page<Order> findAllByPersonId(String id, Pageable pageable);
}
